if($.browser.mozilla||$.browser.opera)
	(function(){
	window.addEventListener('pageshow', PageShowHandler, false);
	window.addEventListener('unload', UnloadHandler, false);
		function PageShowHandler() {
				window.addEventListener('unload', UnloadHandler, false);
		}
		function UnloadHandler() {
				window.removeEventListener('beforeunload', UnloadHandler, false);
		}
})()

$(document).ready(function() {
       $('article').readmore({
        speed: 200,
        maxHeight: 270,
        expandedClass: 'readmore-js-expanded' ,
        moreLink: '<a href="#">Подробнее</a>',
        lessLink: '<a href="#">Скрыть</a>'
    });

});